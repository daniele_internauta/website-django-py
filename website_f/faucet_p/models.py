# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models

class Coinpot(models.Model):
    nome_faucet = models.CharField(max_length=120)
    descrizione = models.TextField(default='descrizione faucet')
    referral_link = models.URLField()
    youtube_link = models.URLField()
    img_logo = models.FileField()
    data = models.DateTimeField(auto_now=False, auto_now_add=True)
    idfaucet = models.CharField(max_length=20)
    slug = models.SlugField(max_length=20)
    m = "T"

    @property
    def modellot(self):
        """Returns the current model T string
       rtype: str
       """
        return "%s%s" % (self.idfaucet, Coinspot.m)

    css = models.TextField(default=Coinpot.modellot)

    def __str__(self):
        return self.nome_faucet
