# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin
from .models import Coinpot
# Register your models here.
class PostAdmin(admin.ModelAdmin):
    list_display = ["__str__","data",]
    list_filter = ["data","nome_faucet"]
    prepopulated_fields = {"slug": ("idfaucet",)}
    p = Coinpot.modellot



    class Meta:
        model = Coinpot

admin.site.register(Coinpot, PostAdmin)
