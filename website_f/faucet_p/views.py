# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.shortcuts import render
from .models import Coinpot
from django.views.generic import ListView
# Create your views here.
def home(request):
     context = locals()
     template = 'home.html'
     return render(request,template,context)
def wallet(request):
     context = locals()
     template = 'wallet.html'
     return render(request,template,context)

def faucet(request):
     context = locals()
     template = "faucet.html"
     return render(request,template,context)
